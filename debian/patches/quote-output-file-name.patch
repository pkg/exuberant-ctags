From 99deb2692099eb0e7cce313ceba17c1eb15d6660 Mon Sep 17 00:00:00 2001
From: Masatake YAMATO <yamato@redhat.com>
Date: Mon, 24 Oct 2016 23:52:23 +0900
Subject: main: quote output file name before passing it to system(3) function

Following command line doesn't work:

      $ ctags -o 'a b' ...

because a shell lauched from system(3) deals a whitespace between 'a'
and 'b' as a separator. The output file name is passed to system(3)
to run external sort command.

This commit adds code to put double and single quoets around the output
file name before passing it to system(3).

The issue is reported by Lorenz Hipp <lhipp@idealbonn.de> in a private mail.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>

Origin: backport, https://github.com/universal-ctags/ctags/commit/e00c55d7a0204dc1d0ae316141323959e1e16162
Bug-Debian: https://bugs.debian.org/1026995
Last-Update: 2022-12-26

Patch-Name: quote-output-file-name.patch
---
 sort.c | 53 ++++++++++++++++++++++++++++++++++++++++++-----------
 1 file changed, 42 insertions(+), 11 deletions(-)

diff --git a/sort.c b/sort.c
index c58defc34..260fbbd21 100644
--- a/sort.c
+++ b/sort.c
@@ -53,17 +53,44 @@ extern void catFile (const char *const name)
 # define PE_CONST const
 #endif
 
+/*
+   Output file name should not be evaluated in system(3) function.
+   The name must be used as is. Quotations are required to block the
+   evaluation.
+
+   Normal single-quotes are used to quote a cstring:
+   a => 'a'
+   " => '"'
+
+   If a single-quote is included in the cstring, use double quotes for quoting it.
+   ' => ''"'"''
+*/
+static void appendCstringWithQuotes (vString *dest, const char* cstr)
+{
+	const char* o;
+
+	vStringPut (dest, '\'');
+	for (o = cstr; *o; o++)
+	{
+		if (*o == '\'')
+			vStringCatS (dest, "'\"'\"'");
+		else
+			vStringPut (dest, *o);
+	}
+	vStringPut (dest, '\'');
+}
+
 extern void externalSortTags (const boolean toStdout)
 {
 	const char *const sortNormalCommand = "sort -u -o";
 	const char *const sortFoldedCommand = "sort -u -f -o";
 	const char *sortCommand =
 		Option.sorted == SO_FOLDSORTED ? sortFoldedCommand : sortNormalCommand;
+# ifndef HAVE_SETENV
 	PE_CONST char *const sortOrder1 = "LC_COLLATE=C";
 	PE_CONST char *const sortOrder2 = "LC_ALL=C";
-	const size_t length = 4 + strlen (sortOrder1) + strlen (sortOrder2) +
-			strlen (sortCommand) + (2 * strlen (tagFileName ()));
-	char *const cmd = (char *) malloc (length + 1);
+# endif
+	vString *cmd = vStringNew ();
 	int ret = -1;
 
 	if (cmd != NULL)
@@ -73,21 +100,25 @@ extern void externalSortTags (const boolean toStdout)
 #ifdef HAVE_SETENV
 		setenv ("LC_COLLATE", "C", 1);
 		setenv ("LC_ALL", "C", 1);
-		sprintf (cmd, "%s %s %s", sortCommand, tagFileName (), tagFileName ());
 #else
 # ifdef HAVE_PUTENV
 		putenv (sortOrder1);
 		putenv (sortOrder2);
-		sprintf (cmd, "%s %s %s", sortCommand, tagFileName (), tagFileName ());
 # else
-		sprintf (cmd, "%s %s %s %s %s", sortOrder1, sortOrder2, sortCommand,
-				tagFileName (), tagFileName ());
+		vStringCatS (cmd, sortOrder1);
+		vStringPut (cmd, ' ');
+		vStringCatS (cmd, sortOrder2);
+		vStringPut (cmd, ' ');
 # endif
 #endif
-		verbose ("system (\"%s\")\n", cmd);
-		ret = system (cmd);
-		free (cmd);
-
+		vStringCatS (cmd, sortCommand);
+		vStringPut (cmd, ' ');
+		appendCstringWithQuotes (cmd, tagFileName ());
+		vStringPut (cmd, ' ');
+		appendCstringWithQuotes (cmd, tagFileName ());
+		verbose ("system (\"%s\")\n", vStringValue (cmd));
+		ret = system (vStringValue (cmd));
+		vStringDelete (cmd);
 	}
 	if (ret != 0)
 		error (FATAL | PERROR, "cannot sort tag file");
